package base.commandPattern;

public class CommandInvoker {
	private Order doCommand, undoCommand;
	public CommandInvoker(AddADominoCommand doCommand) {
		this.doCommand = doCommand;
	}
	
	public CommandInvoker(AddADominoCommand doCommand, UndoADominoCommand undoCommand) {
		this.doCommand = doCommand;
		this.undoCommand = undoCommand;
	}
	
	public void setUndoInvoker(UndoADominoCommand undoCommand) {
		this.undoCommand = undoCommand;
	}
	
	public void doPlace() {
		doCommand.execute();
	}
	
	public void undoPlace() {
		undoCommand.execute();
	}

}
