package base.commandPattern;

public class UndoADominoCommand implements Order{
	private PuttingADomino undoplacedomino;
	
	public UndoADominoCommand(PuttingADomino placedomino) {
		this.undoplacedomino = placedomino;
	}

	@Override
	public void execute() {
		undoplacedomino.unplaceDomino();
	}

}
