package base.commandPattern;

public interface Order {
	void execute();
}
