package base.commandPattern;

public class AddADominoCommand implements Order{
	private PuttingADomino doplacedomino;
	
	public AddADominoCommand(PuttingADomino placedomino) {
		this.doplacedomino = placedomino;
	}

	@Override
	public void execute() {
		doplacedomino.placeDomino();
	}
}
